# Nombre de microservicio
Preregistros
 
## Uso

Instalar las dependencias mediante

En caso de que se quieran saltar las pruebas unitarias aplicar este comando con mvn 

```
mvn clean package -Dmaven.test.skip=true
```

En caso de que no requiera saltar pruebas unitarias realizar este comando

```
mvn clean package
```


## Variables de ambiente

Previo a la ejecucion del programa es necesario configurar variables de ambiente



```
 HOST_MONGO=172.17.0.2
 PUERTO_MONGO=27017
 BASEDATOS_MONGO=Prueba
```

### Puerto donde se encuentra este microservicio

```
server.port=8082 
```

## Ejecucion
```
java -jar microservicio-0.0.1-SNAPSHOT.jar
```
 

